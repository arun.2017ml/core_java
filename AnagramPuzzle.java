import java.util.*;



/**
 * Program that solves the below problem
 * Given two strings,  and , that may or may not be of the same length, determine the minimum number of character deletions required to make  and  anagrams.
 * Any characters can be deleted from either of the strings.
 */
class AnagramPuzzle
{
    public static void main(String[] args)
    {
        String string1 = "cde";
        String string2 = "abc";

        Set<Character> charSet = new HashSet<>();

        Map<Character, Integer> charMap1 = populateCharMap(string1, charSet);
        Map<Character, Integer> charMap2 = populateCharMap(string2, charSet);

        System.out.println(charMap1);
        System.out.println(charMap2);

        System.out.println(charSet);
        int count = 0;
        for(Character ch: charSet){
            if(charMap1.containsKey(ch) && charMap2.containsKey(ch)){
                count+=Math.abs(charMap1.get(ch) - charMap2.get(ch));
            } else if (charMap1.containsKey(ch)) {
                count+=charMap1.get(ch);
            } else if (charMap2.containsKey(ch)){
                count+=charMap2.get(ch);
            }

        }

        System.out.println(charMap1.equals(charMap2));
        System.out.println("no of characters  =  " + count);
    }


    private static Map<Character, Integer> populateCharMap(String s, Set<Character> charSet)
    {
        Map<Character, Integer> charMap = new HashMap<Character, Integer>();
        for (Character ch : s.toCharArray())
        {
            if(ch.equals(' ')){
                continue;
            }
            charSet.add(ch);
            if(charMap.containsKey(ch))
            {
                charMap.put(ch, charMap.get(ch) + 1);
            }
            else
            {
                charMap.put(ch, 1);
            }
        }
        return charMap;

    }
    
}