import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

class Anagram
{
    public static void main(String[] args)
    {
        String string1;
        String string2;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter the two Strings to check if they are Anagrams ");
        string1 = scanner.next();
        string2 = scanner.next();

        Map<Character, Integer> charMap1 = populateCharMap(string1);
        Map<Character, Integer> charMap2 = populateCharMap(string2);

        System.out.println(charMap1);
        System.out.println(charMap2);
        System.out.println(charMap1.equals(charMap2));
    }

    private static Map<Character, Integer> populateCharMap(String s)
    {
        Map<Character, Integer> charMap = new HashMap<Character, Integer>();
        for (Character ch : s.toCharArray())
        {
            if(charMap.containsKey(ch))
            {
                charMap.put(ch, charMap.get(ch) + 1);
            }
            else
            {
                charMap.put(ch, 1);
            }
        }
        return charMap;

    }
}