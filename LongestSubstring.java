import java.util.Set;
import java.util.HashSet;

/**
 * Given a string, find the length of the longest substring without repeating characters.
 */

class LongestSubstring
{
    public static void main(String[] args)
    {
        String inputString = "b";
        String longSubstring = "";
        Set<Character> charSet;
        for(int i = 0; i <= inputString.length(); i++)
        {
            String partString  = inputString.substring(i, inputString.length());
            charSet = new HashSet<>();
            String tempString = "";
            for(Character ch : partString.toCharArray())
            {   
                if(charSet.contains(ch)) {
                    break;
                }
                else{
                    charSet.add(ch);
                    tempString = tempString + ch;
                    longSubstring = tempString.length() > longSubstring.length() ? tempString : longSubstring;
                }
                System.out.println("charset = " + charSet);
            }
        }
        System.out.println("longest = " + longSubstring);
    }
}