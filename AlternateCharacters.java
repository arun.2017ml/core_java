
/**
* Alternating Characters - number of deletions required not to have repeating alternating characters
*
*/
class AlternateCharacters {
	public static void main(String[] args) {
		
		System.out.println("Hello");
		String s = "aabbccc";

		int count = 0;
		char temp = ' ';
		for(Character ch: s.toCharArray()) {
			if(temp == ch) {
				count++;
			}
			temp = ch;
		}

		System.out.println("no of characters to be deleted " + count);

	}
}